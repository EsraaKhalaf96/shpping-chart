import React, { useEffect, useState } from "react";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Products from "./components/Products/Product";
import data from "./data.json";
import "./App.css";
import ProductModal from "./components/Products/ProductModal";
import Filter from "./components/Filter/Filter";
import Chart from "./components/Chart/Chart";

export default function App() {
  const [products, setProducts] = useState(data);
  const [orderVal, setOrderVal] = useState("");
  const [sizeVal, setSizeVal] = useState("");
  const [cartItems, setChartItems] = useState(JSON.parse(localStorage.getItem('cartItems'))||[]);
  let filtred;
  let TotalPrices;
 useEffect (()=>{
    localStorage.setItem("cartItems",JSON.stringify(cartItems))
 },[cartItems])
  const handelSortOrder = (e) => {
    let Order = e.target.value;
    setOrderVal(Order);
    let sortedProducts = products.sort((a, b) => {
      if (Order === "lowest") {
        return a.price - b.price; //-1 negative number so a before b
      } else if (Order === "highest") {
        return b.price - a.price; // +1 positive number so b before a
      } else {
        return a.id < b.id ? 1 : -1;
      }
    });
    setProducts(products);
  };
  const handelOrderSize = (e) => {
    //debugger;
    // setProducts(data)
   let cloneProducts = [...data];
    setSizeVal(e.target.value)
    if(e.target.value === "ALL"){
       setProducts(data);
    }
    else{
      filtred  = cloneProducts.filter(prod=>prod.sizes.indexOf(e.target.value)!== -1)
      setProducts(filtred)
    }
   
  };
  const addToChart = (newCartItem)=> {
    let clonedChartItems = [...cartItems]
    
     let isExist = clonedChartItems.some(item => item.id === newCartItem.id)
     //if item  exixst will not addded and increament quantity
      if(isExist){
        clonedChartItems.forEach(item =>{
          if(item.id===newCartItem.id){
               item.qty++
          }
        
        })
        setChartItems([...clonedChartItems])
      }
      //if item not  exixst will  addded in
      else{
        setChartItems([...cartItems, newCartItem])
      }   
  }

  const removeFromChart = (id)=> {
  let clonedChartItems = [...cartItems]
   let  filtredItems = clonedChartItems.filter((chartItem)=>chartItem.id !== id)
    setChartItems(filtredItems)
  }
  return (
    <div className="layout">
      <Header />
      <ProductModal />
      <main>
        <div className="wrapper">
          <div className="products-wrapper">
          {/* <button onClick={deleteAllProducts}>delete</button> */}
            <Products  products={products} addToChart={addToChart}/>
          </div>
          <Filter products={products} handelSortOrder={handelSortOrder} orderVal={orderVal}  sizeVal={sizeVal} handelOrderSize={handelOrderSize}/>
 
        </div>
         <Chart chartItems={cartItems} removeFromChart={removeFromChart} TotalPrices={TotalPrices} />
      </main>
      <Footer />
    </div>
  );
}
