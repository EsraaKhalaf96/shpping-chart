import React, { useState } from "react";
import Input from "../Input/Input";
import { words } from '../../word';
import  './CheckOutForm.scss';
import Zoom from 'react-reveal/Zoom'

const CheckOutForm = (props) => {
  return (
    <>
  <div className="checkout-form">
        <span className="close-icon" onClick={() => props.setShowForm(false)}> &times; </span> 
        <Zoom left>

            <form onSubmit={props.submitOrder}>
                <Input
                    label={words.name}
                    type="text"
                    name="name"
                    onChange={props.handleChange}
                />
                <Input 
                    label="Email"
                    type="email"
                    onChange={props.handleChange}
                    name="email"
                />
                                    
                <div>
                    <button type="submit"> {words.checkout} </button>
                </div>
            </form>
            </Zoom>
    </div>
</>
  );
};
export default CheckOutForm;
