import React, { useState } from "react";
import "./Filter.scss";
import Flip from 'react-reveal/Flip';

const Filter = ({handelSortOrder,orderVal,handelOrderSize,sizeVal,products}) => {
  // console.log(product)
  
  return (
    <Flip left>
    <div className="filter-wrapper">
      <h2 className="filter-title"> Filter </h2>
      <div className="num-of-products">{products?.length} Products</div>
      <div className="filter-by-size">
        <span>Filter</span>
        <select className="filter-select" value={sizeVal} onChange={handelOrderSize}>
          <option value="ALL">ALL</option>
          <option value="XS">XS</option>
          <option value="S">S</option>
          <option value="M">M</option>
          <option value="L">L</option>
          <option value="XL">XL</option>
          <option value="XXL">XXL</option>
        </select>
      </div>
      <div className="filter-by-size">
        <span>Order</span>
        <select className="filter-select" value={orderVal} onChange={handelSortOrder}>
          <option value="latest">Latest</option>
          <option value="lowest">lowest</option>
          <option value="highest">Highest</option>
        </select>
      </div>
    </div>
    </Flip>
  );
};
export default Filter;
