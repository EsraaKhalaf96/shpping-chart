import React, { useState } from "react";
import CheckOutForm from "../CheckOutForm/CheckOutForm";
import Bounce from 'react-reveal/Bounce'

import "./Chart.scss";
const Chart = ({ chartItems ,removeFromChart}) => {
   const [ showform, setShowForm] = useState(false);
   const [value, setValue] = useState("")

   const showCheckOutForm = () =>{
    setShowForm(!showform)
   }
   const submitOrder = (e) => {
    e.preventDefault();
    const order = {
        name : value.name,
        email: value.email
    }
    console.log(order)
}
const handleChange = (e) => {
  setValue( (prevState) => ({ ...prevState, [e.target.name]: e.target.value}))
}
  return (
    <div className="cart-wrapper">
      <div className="cart-title">{chartItems.length === 0 ? "card is empty " : <p> there is {chartItems.length} in products</p> }</div>
      <Bounce bottom cascade>
      <div className="cart-items">
        {chartItems.map((chartItem) => (
          <div className="cart-item" key={chartItem.id}>   
            <img src={chartItem.imageUrl} alt="" />
            <div className="cart-info">
              <div>
                <p> title: {chartItem.title} </p>
                <p> qty:{chartItem.qty} </p>
                <p> price: {chartItem.price} </p>
       
              </div>
              <button onClick={() =>removeFromChart(chartItem.id)}>remove</button>
            </div>

          </div>
        ))}
      </div>
      </Bounce>
      <div className="checkout" >
      {chartItems.length !==0 && <p>TotalPrice:{chartItems.reduce((acc,p)=>{
                  return acc + p.price*p.qty
     },0)}</p> }
        <button onClick={showCheckOutForm}>checkout</button>
      </div>
      {/* checkoutForm */}
      {showform &&
             <CheckOutForm 
               setShowForm ={setShowForm}
                submitOrder={submitOrder} 
                handleChange={handleChange}
            />
      }
     
  
    </div>
  );
};
export default Chart;
