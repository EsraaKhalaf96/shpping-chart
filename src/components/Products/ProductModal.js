import React, { useState } from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";

const ProductModal = ({product,closeModal,modalIsOpen}) => {
  // console.log(product)
  return (
    <div>
      <Modal onRequestClose={closeModal} ariaHideApp={false} isOpen={modalIsOpen}>
      <button onClick={closeModal}>close</button>
        <div>
           <img src={product?.imageUrl} />
            <div className="product-desc">
              <p>{product?.title}</p>
              <span>{product?.price}</span>
            </div>
          
        </div>
      </Modal>
  
    </div>
  );
};
export default ProductModal;
