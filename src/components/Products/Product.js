import React, { useState, useEffect } from "react";
import "./Products.scss";
import Modal from "react-modal";
import ProductModal from "./ProductModal";
import Bounce from 'react-reveal/Bounce';

const Products = ({ products, addToChart,cartItems }) => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [product, setproduct] = useState([]);
  const openModal = (product) => {
    setproduct(product);
    setIsOpen(true);
  };
  const closeModal = () => {
    setIsOpen(false);
    // console.log
  };
  return (
    <Bounce left cascade>
    <div className="products-wrapper">
      {products.map((product) => {
        return (
          // <div className="product-item" key={product.id} onClick={() => openModal(product)} >
          <div
            className="product-item"
            key={product.id}
   
          >
            <img src={product.imageUrl} onClick={() => openModal(product)}/>
            <div className="product-desc">
              <p>{product.title}</p>
              <span>${product.price}</span>
            </div>
            <button onClick={() => addToChart(product)} >Add to chart</button>
          </div>
        );
      })}
     
      <ProductModal
        product={product}
        closeModal={closeModal}
        modalIsOpen={modalIsOpen}
      />
      {/* <Modal onRequestClose={closeModal} ariaHideApp={false} isOpen={modalIsOpen}>
      <button onClick={closeModal}>close</button>
        <div>
           <img src={product.imageUrl} />
            <div className="product-desc">
              <p>{product.title}</p>
              <span>{product.price}</span>
            </div>
          
        </div>
      </Modal> */}
    </div>
    </Bounce>
  );
};
export default Products;
